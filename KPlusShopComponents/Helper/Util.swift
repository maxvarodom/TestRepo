import UIKit

func currentLanguage() -> String {
	let languages:[String] = UserDefaults.standard.object(forKey: "AppleLanguages") as! [String]
	let currentLanguage = languages[0]
	return currentLanguage
}

