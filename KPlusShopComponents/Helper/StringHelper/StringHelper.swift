import Foundation

class StringHelper {
	
	static let shared = StringHelper()
	let numberFormatter = NumberFormatter()
	
	init() {
		numberFormatter.minimumFractionDigits = 2
		numberFormatter.maximumFractionDigits = 2
		numberFormatter.numberStyle = .decimal
	}
}
