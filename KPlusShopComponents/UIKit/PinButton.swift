import UIKit

open class PinButton: UIButton {
	override open var isHighlighted: Bool {
		didSet {
			if isHighlighted {
				self.titleLabel?.alpha = 1.0
				backgroundColor = UIColor.button.green
			}
			else{
				backgroundColor = UIColor.white
			}
		}
	}
}
