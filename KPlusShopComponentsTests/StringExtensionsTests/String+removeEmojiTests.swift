@testable import KPlusShopComponents
import XCTest

class String_removeEmojiTests: XCTestCase {
    
	func testremoveEmojiReturnText() {
		let str1 = "Hello, my name is Jason 😀 🐸 how are you ?"
		let str2 = "Hello, my name is 😀 Jasonl 😀"
		let str3 = "Hello, my name is 😀 Jason.  👏"
		let str4 = "I am going to the ⛱beach with some 🙉monkeys 👏"

		XCTAssertEqual(str1.removeEmoji, "Hello, my name is Jason   how are you ?")
		XCTAssertEqual(str2.removeEmoji, "Hello, my name is  Jasonl ")
		XCTAssertEqual(str3.removeEmoji, "Hello, my name is  Jason.  ")
		XCTAssertEqual(str4.removeEmoji, "I am going to the beach with some monkeys ")
	}
}
