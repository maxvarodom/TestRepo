@testable import KPlusShopComponents
import XCTest

class StringExtensionTests_: XCTestCase {
	
	func testExtensionStringContainsEmojiReturnFalse() {
		let str1 = "Hello, my name is Jason 😀 🐸 how are you ?"
		let str2 = "Hello, my name is 😀 Jasonl 😀"
		let str3 = "Hello, my name is 😀 Jason.  👏"
		let str4 = "I am going to the ⛱beach with some 🙉monkeys 👏"
		
		XCTAssertFalse(str1.containsEmoji)
		XCTAssertFalse(str2.containsEmoji)
		XCTAssertFalse(str3.containsEmoji)
		XCTAssertFalse(str4.containsEmoji)
	}
	
	func testExtensionStringContainsEmojiReturnFalseReturnTrue() {
		let str1 = "Hello, my name is Jason how are you ?"
		let str2 = "Hello, my name is Jason"
		let str3 = "Hello, my name is Jason"
		let str4 = "I am going to the beach with some monkeys "
		
		XCTAssertTrue(str1.containsEmoji)
		XCTAssertTrue(str2.containsEmoji)
		XCTAssertTrue(str3.containsEmoji)
		XCTAssertTrue(str4.containsEmoji)
	}
}
