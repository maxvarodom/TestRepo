@testable import KPlusShopComponents
import XCTest

class VaridateFieldComponentsTests: XCTestCase {
	
	func testValidateEmojiReturnFalse() {
		let str1 = "Hello, my name is Jason 😀 🐸 how are you ?"
		let str2 = "Hello, my name is 😀Jason"
		let str3 = "Hello, my name is 😀 Jason"
		let str4 = "I am going to the ⛱beach with some 🙉monkeys 👏"
		
		let test1 = VaridateFieldComponents.init(in: str1, command: [.emoji]).validate()
		XCTAssertFalse(test1)
		let test2 = VaridateFieldComponents.init(in: str2, command: [.emoji]).validate()
		XCTAssertFalse(test2)
		let test3 = VaridateFieldComponents.init(in: str3, command: [.emoji]).validate()
		XCTAssertFalse(test3)
		let test4 = VaridateFieldComponents.init(in: str4, command: [.emoji]).validate()
		XCTAssertFalse(test4)
		
		print(str4.removeEmoji)
	}
	
	func testValidateEmojiReturnTrue() {
		let str1 = "Hello, my name is Jason how are you ?"
		let str2 = "Hello, my name is Jason"
		let str3 = "Hello, my name is Jason"
		let str4 = "I am going to the beach with some monkeys "
		let str5 = ""
		
		let test1 = VaridateFieldComponents.init(in: str1, command: [.emoji]).validate()
		XCTAssertTrue(test1)
		let test2 = VaridateFieldComponents.init(in: str2, command: [.emoji]).validate()
		XCTAssertTrue(test2)
		let test3 = VaridateFieldComponents.init(in: str3, command: [.emoji]).validate()
		XCTAssertTrue(test3)
		let test4 = VaridateFieldComponents.init(in: str4, command: [.emoji]).validate()
		XCTAssertTrue(test4)
		let test5 = VaridateFieldComponents.init(in: str5, command: [.emoji]).validate()
		XCTAssertTrue(test5)
	}
}
